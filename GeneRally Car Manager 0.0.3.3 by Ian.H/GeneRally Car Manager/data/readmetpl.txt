GeneRally Car :- [#RELEASENAME#] (v[#RELEASEVERSION#])
Copyright (c) 2005, [#CREATORNAME#]

Release Date: [#RELEASEDATE#]

Car Files:
[#CARFILES#]

This car MAY NOT be edited or re-released in any format,
without previous permission.

[#CREATORNAME#]
[#CREATORWEBSITE#]