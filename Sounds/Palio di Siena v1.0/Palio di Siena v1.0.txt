
				Sound set for Palio di Siena (horse racing)


Description:	Palio di Siena sound set, exclusively made for the Palio di Siena competition
		Palio di Siena website: http://www.villageparty.it/AC05/grpds/index.htm

Version:	1.0
Author:		LongBow
eMail:		longbow@volja.net
Web site:	http://longbow.vze.com/
Released:	September 16, 2005
Legal Note:	This soundset may be distributed with notification -
		provided this readme file is included and with all files left intact 
		(no editing in any way)
		Copyright � 2005 LongBow - All rights reserved.


To use the sound set in GeneRally, you must first copy the Palio di Siena v1.0 folder in the Sounds folder
which can be found in the main GeneRally folder. (ex: GeneRally\Sounds\Palio di Siena v1.0)
Once you have done this, simply run GR and in the option\advanced select the Palio di Siena v1.0.
