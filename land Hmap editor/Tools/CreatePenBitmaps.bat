@rem  -pen fname,type,startAngle,endAngle,rampInAngle,rampOutAngle,inRadius,outRadius,surfaceCurvature,contrast
@rem  type: 4-bit code (where bit 0=low,1=high) &1=inside, &2=inside radius, &4=outside radius, &8=outside, &16=ramp-in value, &32=ramp-out value
@rem        for example: 1+2=3=round pen, 4=banking, 2+4=6=curved rectangle, 4+16+32=52=banking with ramp down&up, 4+32=36=banking with ramp up&up

set PROG=..\GRHMEdit.exe
%PROG% -pen round1.bmp 3   0 360 0 0   0 60 0.1 1.3
%PROG% -pen round2.bmp 3   0 360 0 0   0 60 0.4 1.2
%PROG% -pen round3.bmp 3   0 360 0 0   0 60 0.8 1
%PROG% -pen round4.bmp 3   0 360 0 0   0 60 2.0 1
%PROG% -pen round5.bmp 3   0 360 0 0   0 60 5.0 1
%PROG% -pen round6.bmp 3   0 360 0 0   0 60 20.0 1
%PROG% -pen round7.bmp 3   0 360 0 0   0 60 100.0 1

%PROG% -pen banking180_1.bmp 4   0 180 0 0   50 100 1.0 1.2
%PROG% -pen banking180_2.bmp 4   0 180 0 0   30 100 1.0 1.15
%PROG% -pen banking180_3.bmp 4   0 180 0 0   40 100 2.0 1.5
%PROG% -pen banking180_4.bmp 4   0 180 0 0   50 100 2.5 1.9
%PROG% -pen banking120_1.bmp 4   0 120 0 0   50 100 1.0 1.2
%PROG% -pen banking120_2.bmp 4   0 120 0 0   30 100 1.0 1.15
%PROG% -pen banking120_3.bmp 4   0 120 0 0   40 100 2.0 1.5
%PROG% -pen banking120_4.bmp 4   0 120 0 0   50 100 2.5 1.9
%PROG% -pen banking90_1.bmp 4   0 90 0 0   50 100 1.0 1.2
%PROG% -pen banking90_2.bmp 4   0 90 0 0   30 100 1.0 1.15
%PROG% -pen banking90_3.bmp 4   0 90 0 0   40 100 2.0 1.5
%PROG% -pen banking90_4.bmp 4   0 90 0 0   50 100 2.5 1.9
%PROG% -pen banking60_1.bmp 4   0 60 0 0   50 100 1.0 1.2
%PROG% -pen banking60_2.bmp 4   0 60 0 0   30 100 1.0 1.15
%PROG% -pen banking60_3.bmp 4   0 60 0 0   40 100 2.0 1.5
%PROG% -pen banking60_4.bmp 4   0 60 0 0   50 100 2.5 1.9
%PROG% -pen banking15_1.bmp 4   0 15 0 0   50 100 1.0 1.2
%PROG% -pen banking15_2.bmp 4   0 15 0 0   30 100 1.0 1.15
%PROG% -pen banking15_3.bmp 4   0 15 0 0   40 100 2.0 1.5
%PROG% -pen banking15_4.bmp 4   0 15 0 0   50 100 2.5 1.9

%PROG% -pen bankingWithRampUp180_1.bmp 4   0 180 20 20   50 100 1.0 1.2
%PROG% -pen bankingWithRampUp180_2.bmp 4   0 180 20 20   30 100 1.0 1.15
%PROG% -pen bankingWithRampUp180_3.bmp 4   0 180 20 20   40 100 2.0 1.5
%PROG% -pen bankingWithRampUp180_4.bmp 4   0 180 20 20   50 100 2.5 1.9
%PROG% -pen bankingWithRampUp120_1.bmp 4   0 120 20 20   50 100 1.0 1.2
%PROG% -pen bankingWithRampUp120_2.bmp 4   0 120 20 20   30 100 1.0 1.15
%PROG% -pen bankingWithRampUp120_3.bmp 4   0 120 20 20   40 100 2.0 1.5
%PROG% -pen bankingWithRampUp120_4.bmp 4   0 120 20 20   50 100 2.5 1.9
%PROG% -pen bankingWithRampUp90_1.bmp 4   0 90 20 20   50 100 1.0 1.2
%PROG% -pen bankingWithRampUp90_2.bmp 4   0 90 20 20   30 100 1.0 1.15
%PROG% -pen bankingWithRampUp90_3.bmp 4   0 90 20 20   40 100 2.0 1.5
%PROG% -pen bankingWithRampUp90_4.bmp 4   0 90 20 20   50 100 2.5 1.9
%PROG% -pen bankingWithRampUp60_1.bmp 4   0 60 20 20   50 100 1.0 1.2
%PROG% -pen bankingWithRampUp60_2.bmp 4   0 60 20 20   30 100 1.0 1.15
%PROG% -pen bankingWithRampUp60_3.bmp 4   0 60 20 20   40 100 2.0 1.5
%PROG% -pen bankingWithRampUp60_4.bmp 4   0 60 20 20   50 100 2.5 1.9

%PROG% -pen bankingRampUp30_1.bmp 4   0 30 30 0   50 100 1.0 1.2
%PROG% -pen bankingRampUp30_2.bmp 4   0 30 30 0   30 100 1.0 1.15
%PROG% -pen bankingRampUp30_3.bmp 4   0 30 30 0   40 100 2.0 1.5
%PROG% -pen bankingRampUp30_4.bmp 4   0 30 30 0   50 100 2.5 1.9
%PROG% -pen bankingRampDn30_1.bmp 4   0 30 0 30   50 100 1.0 1.2
%PROG% -pen bankingRampDn30_2.bmp 4   0 30 0 30   30 100 1.0 1.15
%PROG% -pen bankingRampDn30_3.bmp 4   0 30 0 30   40 100 2.0 1.5
%PROG% -pen bankingRampDn30_4.bmp 4   0 30 0 30   50 100 2.5 1.9

%PROG% -pen curvedrect180_1.bmp 6   0 180 0 0   50 100 1.0 1.0
%PROG% -pen curvedrect180_2.bmp 6   0 180 0 0   70 100 1.0 1.0
%PROG% -pen curvedrect180_3.bmp 6   0 180 0 0   90 100 1.0 1.0
%PROG% -pen curvedrect120_1.bmp 6   0 120 0 0   50 100 1.0 1.0
%PROG% -pen curvedrect120_2.bmp 6   0 120 0 0   70 100 1.0 1.0
%PROG% -pen curvedrect120_3.bmp 6   0 120 0 0   90 100 1.0 1.0
%PROG% -pen curvedrect90_1.bmp 6   0 90 0 0   50 100 1.0 1.0
%PROG% -pen curvedrect90_2.bmp 6   0 90 0 0   70 100 1.0 1.0
%PROG% -pen curvedrect90_3.bmp 6   0 90 0 0   90 100 1.0 1.0
%PROG% -pen curvedrect60_1.bmp 6   0 60 0 0   50 100 1.0 1.0
%PROG% -pen curvedrect60_2.bmp 6   0 60 0 0   70 100 1.0 1.0
%PROG% -pen curvedrect60_3.bmp 6   0 60 0 0   90 100 1.0 1.0
%PROG% -pen curvedrect15_1.bmp 6   0 15 0 0   50 100 1.0 1.0
%PROG% -pen curvedrect15_2.bmp 6   0 15 0 0   70 100 1.0 1.0
%PROG% -pen curvedrect15_3.bmp 6   0 15 0 0   90 100 1.0 1.0
