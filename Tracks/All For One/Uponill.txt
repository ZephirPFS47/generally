Uponill
by Danador12
(release date: 20 Oct 2020*)

You need GeneRally 1.2c or higher to play this track.
AI designed for Formula.
length:305; 
World size:100

Uponill is a little, mini track with a lot of curves, a banked hairpin and a gravel hairpin. The last curve is quite difficult to take without lose control.
The middle section of the track, between Checkpoint #4 and #6 is quite difficult without crashing into the sunk tyre.
Anyway, it follows the concept to have less human presence than possible.

Let me know what do you think! :D
*As all the tracks in the trackpack