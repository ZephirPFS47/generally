Yacuzi
by Danador12
(release date: 20 Oct 2020*)

You need GeneRally 1.2c or higher to play this track.
AI designed for Formula.
length:406; 
World size:120

Yacuzi is a funny joke!
It doesn't follows any rule, any line, any idea. It's just the will to make a completely different track.
It has a little main straight, with a curve on the grass, then there are a dive in the pool, from where you are launched into the second part of the track,
a longer straight and then a slowy last part on oil that makes your life harder. By the way of oil, i hope you'll never start on P5, because you're automatically P6: 
too much oil under your tyres.
And talking about tyres, you have to jump on sunk tyres at the entrance and the exit of pitlane.
In short, a mad track for mad races.
Hope you'll have fun with it!
Let me know what do you think! :D

*As all the tracks in the trackpack