Hilly Glade
by Danador12
(release date: 20 Oct 2020*)

You need GeneRally 1.2c or higher to play this track.
AI designed for Formula.
length:384; 
World size:120

Hilly Glade is a little fictional track setted in a glade of a wood.
There are two layouts, normal and reversed, and, as for Fawn, is fully immersed in nature. 
More than Fawn, here there are only one stand, the only presence of humans on the track;
And an house, but the house could be abandoned.
It's a very fast track and the narrow tarmac could be a problem to win easily.
For me, it's a funny track and i love run on it.
Hope you'll enjoy!

*As for all the tracks in the trackpack.