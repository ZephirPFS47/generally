Yhe
by TuomoH
(c)2008
(release date: Oct 29 2008)

You need GeneRally 1.05 or higher to play this track.

Use MONSTERTRUCK!

Yhe	[ofr]	length:266  size:120

Yhe is an offorad course in shape of an oval. Even though the layout is simple, the fight is hard.

You MAY distribute this track freely as long as the zip file (yhe.zip) contains both the track file and this readme file.
However, these files MAY NOT be modified.

Suggestions, bug reports etc.: tuomoh77@gmail.com

My GeneRally tracks: http://tuomoh.rscsites.org
