Seaside v1.0
(c) TuomoH 2003

You need GeneRally 1.03 or higher to play this track.

World size:   105
Track length: 349
Track type:   Street
Recommended car: Formula
Known problems: The narrowness of the track gives hard time for both fast cars and low AI levels.

Seaside is yet another fantasy street track going through narrow streets.
The AI is optimized for Formula around 100. Even with them the hard battle makes the cars crash sometimes, especially in the first corner.
Lower AI levels and faster cars tend to have evenmore problems (the concrete walls are lurking near...)
 
Version history:
1.0 (April 9, 2003)
  First release of the track.

You MAY distribute this track freely as long as the zip file (seaside.zip) contains all the three track files and the readme file.
However, these files MAY NOT be modified.
 
Suggestions, bug reports etc.: stereospace@hotmail.com
My GeneRally tracks: http://tuomoh.rscsites.org
