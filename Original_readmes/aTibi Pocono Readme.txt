===============================================================================
Track              	: Pocono Raceway
Location           	: Long Pond, Pennsylvania, USA
Tags               	: [rwl][ovl][roa]
Filename uploaded  	: Pocono_Raceway_by_AT.zip
Version            	: v1.0
Tracks on zip file 	: Pocono_TTOC.TRK - the Tricky Triangle Oval Course
		   	  Pocono_SC.TRK -  South Course
			  Pocono_RC.TRK - Road Course
			  Pocono_RCC.TRK - Road Course with Chicane
			  Pocono_NC.TRK - North Course
			  Pocono_DIC.TRK - Double Infield Course
			  Pocono_DICA.TRK -  Double Infield Course Alternative	
Release Date       	: December 1st, 2016
Email Address      	: andrea.tibiletti@me.com
Home Page          	: under construction
Other Tracks by Author  : 
Additional Credits      : Best regard
                                
===============================================================================================

* Play Information * and * Construction *

About track	  : Pocono_TTOC.TRK - the Tricky Triangle Oval Course
Game Requirements : GeneRally 1.2c
Track Type        : Oval 
Track Surface     : Tarmac
Track Length      : 722 meters
Rated Difficulty  : Medium
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 225
Water Level           : 25
View Angle            : 80 degrees
Rotation              : 0 degrees
Zoom                  : 96
Number of Checkpoints : 10
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 30 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release

About track	  : Pocono_SC.TRK -  South Course
Game Requirements : GeneRally 1.2c
Track Type        : Semi Oval + Infield Road 
Track Surface     : Tarmac
Track Length      : 350 meters
Rated Difficulty  : Medium
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 225
Water Level           : 25
View Angle            : 80 degrees
Rotation              : 0 degrees
Zoom                  : 96
Number of Checkpoints : 12
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 30 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release

About track	  : Pocono_RC.TRK - Road Course
Game Requirements : GeneRally 1.2c
Track Type        : Semi Oval + Infield Road 
Track Surface     : Tarmac
Track Length      : 710 meters
Rated Difficulty  : Medium
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 225
Water Level           : 25
View Angle            : 80 degrees
Rotation              : 0 degrees
Zoom                  : 96
Number of Checkpoints : 13
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 30 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release

About track	  : Pocono_RCC.TRK - Road Course with Chicane
Game Requirements : GeneRally 1.2c
Track Type        : Semi Oval + Infield Road 
Track Surface     : Tarmac
Track Length      : 714 meters
Rated Difficulty  : Medium
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 225
Water Level           : 25
View Angle            : 80 degrees
Rotation              : 0 degrees
Zoom                  : 96
Number of Checkpoints : 16
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 30 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release

About track	  : Pocono_NC.TRK - North Course
Game Requirements : GeneRally 1.2c
Track Type        : Semi Oval + Infield Road 
Track Surface     : Tarmac
Track Length      : 380 meters
Rated Difficulty  : Medium
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 225
Water Level           : 25
View Angle            : 80 degrees
Rotation              : 0 degrees
Zoom                  : 96
Number of Checkpoints : 10
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 30 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release

About track	  : Pocono_DIC.TRK - Double Infield Course
Game Requirements : GeneRally 1.2c
Track Type        : Semi Oval + Double Infield Road 
Track Surface     : Tarmac
Track Length      : 633 meters
Rated Difficulty  : Medium
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 225
Water Level           : 25
View Angle            : 80 degrees
Rotation              : 0 degrees
Zoom                  : 96
Number of Checkpoints : 16
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 30 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release

About track	  : Pocono_DICA.TRK - Double Infield Course
Game Requirements : GeneRally 1.2c
Track Type        : Semi Oval + Double Infield Road 
Track Surface     : Tarmac
Track Length      : 672 meters
Rated Difficulty  : Medium
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 225
Water Level           : 25
View Angle            : 80 degrees
Rotation              : 0 degrees
Zoom                  : 96
Number of Checkpoints : 17
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 30 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release

 
* Copyright and Permissions *

Authors MAY use this track as a base to build additional tracks.

You MAY distribute this track, provided you include this file, with
no modifications. You MAY distribute this track in any electronic
format (BBS, Web Site, etc...) as long as you include this file intact.

* Where to get this track *

http://forum.generally-racers.com/viewforum.php?f=6&sid=941c163d86cd97bfc4fcde5767ed1cbf

Regards from Italy, Andrea Tibiletti