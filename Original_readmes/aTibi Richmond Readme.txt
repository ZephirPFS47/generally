===============================================================================
Track              	: Richmond International Raceway
Location           	: 600 E Laburnum Ave, Richmond, VA 23222, USA
Tags               	: [rwl][ovl]
Filename uploaded  	: Richmond_International_Raceway_by_AT.zip
Version            	: v1.0
Tracks on zip file 	: Richmond_aT.TRK
Release Date       	: December 29, 2016
Email Address      	: andrea.tibiletti@me.com
Home Page          	: under construction
Other Tracks by Author  : 
Additional Credits      : Best regard
                                
===============================================================================================

* Play Information * and * Construction *

About track	  : Richmond_aT.TRK - Oval Course
Game Requirements : GeneRally 1.2c
Track Type        : Oval 
Track Surface     : Tarmac
Track Length      : 425 meters
Rated Difficulty  : Hard
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 175
Water Level           : 50
View Angle            : 75 degrees
Rotation              : 0 degrees
Zoom                  : 94
Number of Checkpoints : 8
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 30 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release


 
* Copyright and Permissions *

Authors MAY use this track as a base to build additional tracks.

You MAY distribute this track, provided you include this file, with
no modifications. You MAY distribute this track in any electronic
format (BBS, Web Site, etc...) as long as you include this file intact.

* Where to get this track *

http://forum.generally-racers.com/viewforum.php?f=6&sid=941c163d86cd97bfc4fcde5767ed1cbf

Regards from Italy, Andrea Tibiletti