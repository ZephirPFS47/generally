Lakeview Drive
(c) 2020 by TuomoH
Release date: June 05, 2020

You need GeneRally 1.2d or higher to play this track.

Use FORMULA!

LakeviewDr	[roa]	Size:140	Length: 338

Lakeview Drive is a road course that goes around a small lake. It's got some wicked hmap changes along the way.
This circuit has been made 100% without external editors (i.e., Track Editor Only).

You may distribute this track freely as long as the zip (lakeviewdr.zip) is intact and includes both the trk file and this readme.

COMPETITION INFORMATION: You may use any of my tracks in competitions as long as properly credited.
 You may also make necessary edits to this end (checkpoints, speed limits etc.)

Bugs, suggestions etc. tuomoh77@gmail.com
Web: tuomoh.gene-rally.com
My tracks at GeneRally International Forum: http://forum.generally-racers.com/viewtopic.php?f=21&t=4334