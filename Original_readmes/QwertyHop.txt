QwertyHop
by TuomoH
(c)2004
(release date: Aug 29 2004)

You need GeneRally 1.05 or higher to play these tracks.

Use GENERAL!

QwertyHop	[stu]/[oth] length:275 size:100

QwertyHop is a fun race on a work desk and over the computer keyboard, a bit in style of Micro Machines.

You MAY distribute this trackpack freely as long as the zip file (qwertyhop.zip) contains the track file and this readme file.
However, these files MAY NOT be modified.

Suggestions, bug reports etc.: stereospace@hotmail.com

My GeneRally tracks: http://tuomoh.rscsites.org
