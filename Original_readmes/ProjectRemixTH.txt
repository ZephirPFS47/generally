Project Remix TH
by TuomoH (in collaboration with AleksiNir, CamrosX and GRB)
(c)2007
(release date: Sep 3 2007)

You need GeneRally 1.05 or higher to play these track.

Use MINI on ws50 tracks!
Use FORMULA elsewhere!

50-Kololoa	[mic]	length:232 size:50
50-Phoenix	[mic]	length:137 size:50
Carolla		[cir]	length:689 size:205
Lechelle	[roa]	length:533 size:170
Minbeam		[roa]	length:379 size:100
Pareld-C	[cir]	length:398 size:140
Pareld-O	[ovl]	length:321 size:140

This pack includes seven tracks, two completely by me and five in collaboration with others.

50-Kololoa: 	TuomoH
50-Phoenix: 	TuomoH (hmap, AI line, finishing touches); CamrosX (lmap, layout, objects)
Carolla:	TuomoH (layout, basic hmap, basic mud texture); GRB (rest)
Lechelle:	TuomoH (layout); GRB (rest)
Minbeam:	TuomoH
Pareld-C:	TuomoH (layout, hmap, basic track lmap); AleksiNir (rest)
Pareld-O:	TuomoH (layout, hmap, basic track lmap); AleksiNir (rest)

You MAY distribute this trackpack freely as long as the zip file (projectremixth.zip) contains all seven (7) track files and this readme file.
However, these files MAY NOT be modified.

Suggestions, bug reports etc.: stereospace@hotmail.com
My GeneRally tracks: http://tuomoh.rscsites.org

Thank you's go to AleksiNir, CamrosX and GRB.