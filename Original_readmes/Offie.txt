Offie v1.0
(c) TuomoH 2003

You need GeneRally 1.03 or higher to play this track.

World size:   100
Track length: 290
Track type:   Off-road
Recommended car: Strava's SODA set/Monstertruck/General

Just felt like fooling around and this is the result: A short off-road track in the middle of the forest.
The AI is optimized for Strava's SODA set around 100 but works okay with other cars and lower levels as well.
 
Version history:
1.0 (November 21, 2003)
  First release of the track.

You MAY distribute this track freely as long as the zip file (offie.zip) contains both the track file and the readme file.
However, these files MAY NOT be modified.
 
Suggestions, bug reports etc.: stereospace@hotmail.com
My GeneRally tracks: http://tuomoh.rscsites.org
