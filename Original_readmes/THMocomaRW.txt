Mocoma Raceway v1.0
(c) TuomoH 2002

You need GeneRally 1.02 or higher to play this track.

World size:   180
Track length: 720
Track type:   Tarmac raceway
Recommended car: Formula/GP

This is my third track for GeneRally. It's a fictional raceway featuring one banked 180 degree turn. 
The AI works pretty okay with Formula and GP. ChampCarM seems to be sliding too much in two or three places but it still doesn't hit the concrete walls often.

Version history:
1.0 (October 27, 2002)
  First release of the track.

You MAY distribute this track freely as long as the zip file (thmocomarw.zip) contains both the track and the readme file.
However, these files MAY NOT be modified.
 
Suggestions, bug reports etc.: stereospace@hotmail.com
My GeneRally tracks: http://tuomoh.rscsites.org
