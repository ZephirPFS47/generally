             + General Information +
Track                   : Yarky Six
Location                : (fictional track)
Files                   : Yarky Six.trk, Yarky Six.txt
Version                 : v1.00 (released 30-09-2020)
Author                  : XYY
Email address           : xyyverwaltung@gmail.com
Description             : A very old track which was built in 2010 still 
                        : in GeneRally v1.05. In 2014 I continued working
                        : on the track. The finishing touches were made
                        : in 2020. It can be considered as my first track
                        : with some sort of professional build style.
                        : However, it is intentionally not updated to the
                        : sophisticated level I reached afterwards, to
                        : keep the spirit of this 2010 track.

              + Track Information +
Game requirements       : GeneRally 1.2 (or higher)
Track type              : Circuit
Track surface           : Tarmac, Tarmac 2
Track length            : 415 m
World size              : 160
Rated difficulty        : Intermediate

            + Copyright Information +
You may use this track wherever you want if you keep the track file(s)
intact. Redistributing edited versions of the track file(s) is not
allowed. But I may give you an extra permission if you ask me
(best via e-mail).                                           (c) 2020 XYY