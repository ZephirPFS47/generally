Rampland v1.0
(c) TuomoH 2003

You need GeneRally 1.03 or higher to play this track.

World size:   100
Track length: 312
Track type:   Stunt
Recommended car: General (or any similar car)
Known problems: There are still some problems with the AI.

Rampland is my first attempt at creating a stun track. I think it turned out well although the AI has some problems.
The AI is optimized for General around 100 but works okay with lower levels too.
 
Version history:
1.0 (August 11, 2003)
  First release of the track.

You MAY distribute this track freely as long as the zip file (rampland.zip) contains both the track file and the readme file.
However, these files MAY NOT be modified.
 
Suggestions, bug reports etc.: stereospace@hotmail.com
My GeneRally tracks: http://tuomoh.rscsites.org
