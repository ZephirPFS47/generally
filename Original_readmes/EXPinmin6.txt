EXPinmin6 [experiments in minimalism 6]
by TuomoH
(c)2014
(release date: Aug 13 2014)

You need GeneRally 1.2c or higher to play these tracks.

Use MINI!
Don't use too much damage!

EXPdownup	[mic]	length:158  size:50
EXPriverhop	[mic]	length:131  size:50
EXProundown	[mic]	length:208  size:50
EXPtougedrop 	[mic]	length:204  size:50

Expinmin series are back. As usual, we've got chaotic micro tracks designed for the original Mini.
This time the tracks are all tarmac-based and involve some serious hmap action. Therefore, the AI is far from perfect.

You MAY distribute this trackpack freely as long as the zip file (expinmin6.zip) contains all four (4) track files and this readme file.
However, these files MAY NOT be modified.

COMPETITION INFORMATION: All of my tracks can be used in competitions even in modified form (e.g., checkpoints, safepit) as long as properly credited.

Suggestions, bug reports etc.: tuomoh77@gmail.com

My GeneRally tracks: http://tuomoh.gene-rally.com
