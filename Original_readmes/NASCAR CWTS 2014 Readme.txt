NASCAR Camping World Truck Series by puttz
Manufacturer logos are by Herbal

List of drivers:

Chevy Silverado
#00 Cole Custer (Haas Automation)
#02 Tyler Young (Young's Building Systems)
#6 Norm Benning (Unsponsored)
#10 Jennifer Jo Cobb (Grimes Construction)
#15 Mason Mingus (Call 811 Before You Dig)
#21 Joey Coulter (Alamo)
#24 Brendan Newberry (Gunbroker.com)
#30 Ron Hornaday Jr. (Rheem)
#31 Ben Kennedy (Heater.com)
#50 TJ Bell (Drivin' for Linemen)
#63 Justin Jennings (LG Seeds)
#75 Caleb Holman (Food Country USA)
#99 Bryan Silas (Bell Trucks America inc.)

Ford F-150
#19 Tyler Reddick (DrawTite)
#29 Ryan Blaney (Cooper Standard)
#80 Jody Knowles (Clayton Signs inc.)
#92 Scott Riggs (BTS Tire)

Toyota Tundra
#05 Jon Wes Townley (Zaxby's)
#7 Brian Ickler (Bullet Liner)
#8 Joe Nemechek (SWM/Smoke-N-Sear)
#13 Jeb Burton (Estes)
#17 Timothy Peters (Red Horse Racing)
#51 Kyle Busch (Dollar General)
#54 Darrel Wallace Jr. (Toyota Care)
#77 German Quiroga (OtterBox)
#88 Matt Crafton (Menard's)
#98 Johnny Sauter (Nextant Aerospace)
