Attanym
(c) 2015 by TuomoH
Release date: May 31, 2015

You need GeneRally 1.2c or higher to play this track.

Use MINI!

Attanym	[oth]	Size:73	Length: 143

Attanym is a 45-minute track, just a little fun Mini romp.

You may distribute this track freely as long as the zip (attanym.zip) is intact and includes both the trk file and this readme.

COMPETITION INFORMATION: You may use any of my tracks in competitions as long as properly credited.
 You may also make necessary edits to this end (checkpoints, speed limits etc.)

Bugs, suggestions etc. tuomoh77@gmail.com
Web: tuomoh.gene-rally.com
My tracks at GeneRally International Forum: http://forum.generally-racers.com/viewtopic.php?f=21&t=4334