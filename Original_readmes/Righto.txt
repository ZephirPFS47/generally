Righto
(c) 2014 by TuomoH
Release date: December 29, 2014

You need GeneRally 1.2c or higher to play this track.

Use FORMULA!

Righto	[stu]	Size:130	Length: 483

Righto is a stunt track situated in an urban environment. It features one jump and long banked right-handers.

You may distribute this track freely as long as the zip (righto.zip) is intact and includes both the trk file and this readme.

COMPETITION INFORMATION: You may use any of my tracks in competitions as long as properly credited.
 You may also make necessary edits to this end (checkpoints, speed limits etc.)

Bugs, suggestions etc. tuomoh77@gmail.com
Web: tuomoh.gene-rally.com