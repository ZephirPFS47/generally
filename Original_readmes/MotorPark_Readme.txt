Author: puttz
Track Name: MotorPark
File Name: MotorPark.trk, MotorPark_C.trk, MotorPark_NC.trk, MotorPark_O.trk
Game Version: 1.2c
Version: 1.0
Layouts included: Full, Road without busstop, oval, oval with busstop

Track Background: A small fictional club circuit in a forested area in the middle of nowhere. Popular with local club racers and has track days where the track allows people to bring their street cars in and drive. Also hosts local short track oval series, including late models and street stocks.

Additional information: 

You may not edit this track without prior permission from the author. You may use this track in compos and edit the standard attributes (checkpoints, starting positions, AI lines) without requesting permission from the author. You may redistribute this track with this readme.