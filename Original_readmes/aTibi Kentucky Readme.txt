===============================================================================
Track              	: Kentucky Speedway
Location           	: 4760 Sparta Pike, Sparta, KY 41086, USA
Tags               	: [rwl][ovl]
Filename uploaded  	: 2018 - [rwl] [ovl] - Kentucky_Speedway.zip
Version            	: v1.0
Tracks on zip file 	: Kentucky.TRK
Release Date       	: july 11, 2018
Email Address      	: andrea.tibiletti@gmail.com
Home Page          	: under construction
Other Tracks by Author  : 
Additional Credits      : 
                                
===============================================================================================

* Play Information * and * Construction *

About track	  : Kentucky.TRK
Game Requirements : GeneRally 1.2c
Track Type        : Oval 
Track Surface     : Tarmac
Track Length      : 517 meters
Rated Difficulty  : Medium
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 215 (1 1/2 miles or 2400 mt)
Water Level           : 0
View Angle            : 60 degrees
Rotation              : 0 degrees
Zoom                  : 92
Number of Checkpoints : 7
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 18 mounth (first layout on 2017-04-02)
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release


 
* Copyright and Permissions *

Authors MAY use this track as a base to build additional tracks.

You MAY distribute this track, provided you include this file, with
no modifications. You MAY distribute this track in any electronic
format (BBS, Web Site, etc...) as long as you include this file intact.

* Where to get this track *

http://forum.generally-racers.com

Regards from Italy, Andrea Tibiletti