A TuomoH trackpack

Tracks from the minimalist phase (2005-2007).

 not included: EXPinMIN [tpk]
91	AncientRally	10.02.2006	ral
92	Ground Play	22.06.2006	off
93	Misty Mt Hop	16.07.2006	off
94	Linko		01.09.2006	mic
 not included: expINmin2 [tpk]
101	Village Road	18.09.2006	roa	3rd in ToM 9/2006
102	Bankston	08.10.2006	stc
103	HouseJump	29.12.2006	stu
104	Chistown	18.05.2007	stc 	3rd in ToM 5/2007
105	Batuba8		19.05.2007	mic
106	Clover3		31.05.2007	mic


You MAY distribute this trackpack freely, as long as the zip file is kept intact. However, any of the files MAY NOT be modified.

COMPETITION INFORMATION: All of my tracks can be used in competitions even in modified form (e.g., checkpoints, safepit) as long as properly credited.

