Relaton v1.0
(c) TuomoH 2004

You need GeneRally 1.05 or higher to play this track.

World size:   180
Track length: 555
Track type:   Street
Recommended car: Formula
Known problems: No other cars tested;)

A street track with less concrete than I usually use.
The AI is optimized for Formula around 100.
 
Version history:
1.0 (January 04, 2004)
  First release of the track.

You MAY distribute this track freely as long as the zip file (relaton.zip) contains both the track file and the readme file.
However, these files MAY NOT be modified.
 
Suggestions, bug reports etc.: stereospace@hotmail.com

My GeneRally tracks: http://tuomoh.rscsites.org

