This Is Not A Track
by TuomoH
(c)2013
(release date: Jan 19 2013)

You need GeneRally 1.2c or higher to play this track.

Use GENERAL!

Not-A-Track	[art]	length:166  size:70

This Is Not A Track...
... it's a piece of art you can race on. Mostly ice but there are also grippy tarmac pieces.

You MAY distribute this track freely as long as the zip file (not-a-track.zip) contains both the track file and this readme file.
However, these files MAY NOT be modified.

COMPETITION INFORMATION: All of my tracks can be used in competitions even in modified form (e.g., checkpoints, safepit) as long as properly credited.

Suggestions, bug reports etc.: tuomoh77@gmail.com


