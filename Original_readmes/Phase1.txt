A TuomoH trackpack

Tracks from the beginning phase (2002-2003).

1	THRing		03.09.2002	cir
2	THCity		29.09.2002	stc
3	Mocoma RW	27.10.2002	cir
4	Pocono RW	03.11.2002	ovl (rwl; 2 AI levels)
5	Money Coast	14.11.2002	stc (2 POV's)
6	Trinopolis	30.11.2002	cir
7	THHotel		14.12.2002	roa
8	THVilla		14.12.2002	roa
9	THCrossing	15.12.2002	stu
10	Jivewood	16.12.2002	stu
11	Riverdale	19.12.2002	stc
12	Road 94		29.12.2002	roa
13	THRally		03.01.2003	ral
14+15	THMineral	12.01.2003	ral+cir
16	THCliff		17.01.2003	ral
17	Thurston	26.02.2003	cir
18	Tinyville	22.03.2003	stc (hard/easy sprint)
19	TinyvilleSW	23.03.2003	ovl
20	THJunction	30.03.2003	stc
21	Thurlington	04.04.2003	stc
22	Seaside		09.04.2003	stc
23	Rollstone	11.04.2003	cir
24	Oasis RW	17.04.2003	cir
25	Nebula Park	19.04.2003	cir
26	Parainen	29.04.2003	roa (my home town)
27+28	Raunchola	10.05.2003	stu (tarmac+mud)
29	Crisco		17.05.2003	stc (versions 1.0 and 1.1; chicane/no chicane)


You MAY distribute this trackpack freely, as long as the zip file is kept intact. However, any of the files MAY NOT be modified.

COMPETITION INFORMATION: All of my tracks can be used in competitions even in modified form (e.g., checkpoints, safepit) as long as properly credited.

