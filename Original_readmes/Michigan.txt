Michigan v1.0
This conversion (c) TuomoH 2003

You need GeneRally 1.03 or higher to play this track.

World size:   150
Track length: 497
Track type:   Street
Recommended car: Formula or Zaekyr's ZFormula.
Known problems: There are still some problems with the AI

Michican is my second track conversion from old game called (Danny Sullivan's) Indy Heat.
The AI is optimized for Formula and ZFormula around 100 but it works okay with lower AI levels too (at least with Formula).
 
Version history:
1.0 (July 29, 2003)
  First release of the track.

You MAY distribute this track freely as long as the zip file (michigan.zip) contains both the track file and the readme file.
However, these files MAY NOT be modified.
 
Suggestions, bug reports etc.: stereospace@hotmail.com
My GeneRally tracks: http://tuomoh.rscsites.org

currently playing: Beck - Bottle of Blues