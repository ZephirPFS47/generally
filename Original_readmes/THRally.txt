THRally v1.0
(c) TuomoH 2003

You need GeneRally 1.03 or higher to play this track.

World size:   180
Track length: 1003
Track type:   Rally track (tarmac/mud)

THRally is my first attempt at creating a rally track.
The AI works well with "rally" type cars (e.g. Strava's Rally GTI and SODA set) around 100. The lap lasts over 30 seconds when using these cars.
The AI works ok with "rally" type cars at lower levels too.

Version history:
1.0 (January 3, 2003)
  First release of the track.

You MAY distribute this track freely as long as the zip file (thrally.zip) contains both the track and the readme file.
However, these files MAY NOT be modified.
 
Suggestions, bug reports etc.: stereospace@hotmail.com
My GeneRally tracks: http://tuomoh.rscsites.org
