SkidArena3
by TuomoH
This conversion (c)2004
(release date: Nov 20 2004)
Original track appears in Acid Software game Skidmarks.

You need GeneRally 1.05 or higher to play this track.

Use MONSTERTRUCK.

SkidArena3	[oth] length:497 size:150

This track is a conversion from '93 Amiga game Skidmarks. The original game didn't have track names, and this particular track was #3 in game menus, hence the title SkidArena3.

You MAY distribute this trackpack freely as long as the zip file (skidarena3.zip) contains the track file and this readme file.
However, these files MAY NOT be modified.

Suggestions, bug reports etc.: stereospace@hotmail.com

My GeneRally tracks: http://tuomoh.rscsites.org
