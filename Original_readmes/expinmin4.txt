expinmin4 [experiments in minimalism 4]
by TuomoH
(c)2008
(release date: Nov 02 2008)

You need GeneRally 1.05 or higher to play these tracks.

Use MINI!
Don't use too much damage!

expTarmacPrk	[mic]	length:44  size:25
expOilPark	[mic]	length:44  size:25
expMudPark	[mic]	length:44  size:25
expGrassPark 	[mic]	length:48  size:25
expSnowPark  	[mic]	length:44  size:25

This pack includes five tracks of world size 25, and they're strictly meant for original Mini. The AI is far from perfect.
The tracks were created during a 2 hour session in May '08.

You MAY distribute this trackpack freely as long as the zip file (expinmin4.zip) contains all five (5) track files and this readme file.
However, these files MAY NOT be modified.

Suggestions, bug reports etc.: tuomoh77@gmail.com

My GeneRally tracks: http://tuomoh.rscsites.org
