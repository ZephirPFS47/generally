*** ZIP Info ***

Name             	: Mirage.zip
Files included		: Mirage.trk
			: Mirage ReadMe.txt
Description             : A cute GR track in the middle of a desert? Hmm, maybe it's just a mirage.
Version History		: v1.0 - 8/11 2014 - First release.

*** Author Info ***

Author(s)		: Emil Patanen
Email Address          	: emilpgr@gmail.com
Other Tracks by Author  : Dortuyi v1.0 (Dortuyi.ZIP), L'Kiaw v1.0 (L Kiaw.ZIP), Epsring v1.0 (Epsring.RAR), Saozbe Raceway v1.0 (Saozbe.RAR), Tojo v1.0 (Tojo.ZIP),
			: Loch Rosamunda v1.0 (Rosamunda.ZIP), Imesque v1.0 (Imesque.ZIP), Ciuthe v1.0 (Ciuthe.ZIP), Cyrano Town v1.1 (Cyrano Town v1.1.ZIP), Khole v1.0 (Khole.ZIP),
			: Wysnaw v1.0 (Wysnaw.ZIP), Loislurso v1.0 (Loislurso.ZIP), Tofa v1.0 (Tofa.ZIP), Goby v1.0 (Goby.ZIP), Bokuv v1.0 (Bokuv.ZIP),
			: Ocupi v1.0 (Ocupi.ZIP), Fang v1.0 (Fang.ZIP), Nigemy v1.0 (Nigemy.ZIP), Hawady v1.1 (Hawady.ZIP), Jybonn v1.0 (Jybonn.ZIP),
			: Merridew v1.0 (Merridew.ZIP), Azmo v1.0 (Azmo.ZIP), Out on park v1.0 (Out on park.ZIP), Tarkus v1.0 (Tarkus.ZIP), Qirtami v1.0 (Qirtami.ZIP)
			: Perenne v1.1 (Perenne.ZIP), Mini Offroadies Pack v1.0 (Mini Offroadies.ZIP), Bakiwa v1.0 (Bakiwa.ZIP), Gormit v1.0 (Gormit.ZIP), Wlalace v1.0 (Wlalace.ZIP),
			: Groon v1.0 (Groon.ZIP), Ukudurri City v1.0 (Ukudurri City.ZIP), Atma v1.0 (Atma.ZIP), Aristillus v1.0 (Aristillus.ZIP), R/C Racing Pack v1.0 (RC Racing Pack.ZIP),
			: Diddywah v1.0 (Diddywah.ZIP), Maorgarnn v1.0 (Maorgarnn.ZIP), Emil's GRAquarelles v1.0 (Emil's GRAquarelles.ZIP), Allen Park v1.0 (Allen Park.ZIP), Tropic Isle v1.0 (Tropic Isle.ZIP),
			: Bone County v1.0 (Bone County.ZIP), England v1.1 (England v1.1.ZIP), Marianne v1.0 (Marianne.ZIP), Snowy Valley v1.0 (Snowy Valley.ZIP), 31-01-2014 v1.0 (31-01-2014.ZIP),
			: Cucamonga v1.0 (Cucamonga.ZIP), Nijunak MSR v1.0 (Nijunak MSR.ZIP), Brahen v1.0 (Brahen.ZIP), Giant Stone Rally v1.1 (Giant Stone Rally.ZIP), Generife v1.0 (Generife.ZIP)

*** Copyright and Permissions ***

Authors MAY NOT use this track as a base to build additional tracks.
You MAY distribute this track, provided you include this file, with
no modifications. You MAY distribute this track in any electronic
format (BBS, Web Site, etc...) as long as you include this file intact.
Promoters of competitions MAY use this track without any special permission.

*** Track Info ***

Track Type              : Micro Oval
Track Surface           : Tarmac
Track Length            : 161 meters
Rated Difficulty        : Easy
World Size              : 70
Water Level             : 50
View Angle              : 70 degrees
Rotation                : 0 degrees
Zoom			: 97

*** Other Info ***

Game Requirements       : GeneRally v1.2 or higher
Additional Credits      : -
Editors Used            : GeneRally Track Editor v1.2, GIMP Portable, GRHMEdit
Known Bugs              : -

*** Where to get this track ***

http://forum.generally-racers.com/viewtopic.php?p=30629#p30629
http://trackdb.planet-generally.de/index.php?page=profile&id=51&mode=trk
http://emilpgr.byethost7.com/index.html