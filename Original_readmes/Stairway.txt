Stairway
by TuomoH
(c)2005
(release date: Jul 23 2005)

You need GeneRally 1.05 or higher to play this track.

Use MINI!
Use LOW or NO damage!

Stairway	[oth]	length:131 size:40

Stairway is a track created to celebrate my three years with GeneRally.
The AI is far from perfect due to small world size and extreme hmap.

You MAY distribute this track freely as long as the zip file (stairway.zip) contains both track files and this readme file.
However, these files MAY NOT be modified.

Suggestions, bug reports etc.: stereospace@hotmail.com

My GeneRally tracks: http://tuomoh.rscsites.org
