Misty Mountain Hop
by TuomoH
(c)2006
(release date: Jul 16 2006)

You need GeneRally 1.05 or higher to play this track.

Use MONSTERTRUCK!

Misty Mountain Hop	[ofr]	length:496 size:200

Misty Mountain Hop is an offroad track that goes over and around a mountain range.
The AI is far from perfect due to very bumpy hmap.

You MAY distribute this track freely as long as the zip file (mistymthop.zip) contains both the track file and this readme file.
However, these files MAY NOT be modified.

Suggestions, bug reports etc.: stereospace@hotmail.com

My GeneRally tracks: http://tuomoh.rscsites.org
