Author: puttz
Track Name: Feldman County Speedway
File Name: Feldman Co.trk
Game Version: 1.2c
Version: 1.1
Layouts included: Feldman County Speedway

Track Background: A small local dirt track in the backwoods of town somewhere in the Southern USA.

Additional Information: There is banking on turns 2 and 3, it's just hard to see. The rough ground around the outside of the track is to discourage drivers from trying to take the car into the grass for a faster line.

1.1 Update: Adjusted pit AI line and pit crew locations.

You may not edit this track without prior permission from the author. You may use this track in compos and edit the standard attributes (checkpoints, starting positions, AI lines) without requesting permission from the author. You may redistribute this track with this readme.