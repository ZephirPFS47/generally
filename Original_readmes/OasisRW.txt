Oasis Raceway v1.0
(c) TuomoH 2003

You need GeneRally 1.03 or higher to play this track.

World size:   100
Track length: 262
Track type:   Tarmac raceway
Recommended car: Formula
Known problems: The AI has problems with faster cars when coming to the finsih straight.

Oasis Raceway is a fantasy raceway with interesting hmap changes.
The AI is optimized for Formula around 100 but it works nicely with lower AI as well.
However, faster and heavier cars than Formula don't like much the last corner.
 
Version history:
1.0 (April 17, 2003)
  First release of the track.

You MAY distribute this track freely as long as the zip file (oasisrw.zip) contains all the three track files and the readme file.
However, these files MAY NOT be modified.
 
Suggestions, bug reports etc.: stereospace@hotmail.com
My GeneRally tracks: http://tuomoh.rscsites.org
