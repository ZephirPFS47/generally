===============================================================================
Track              	: Jim Hall Kart Racing School
Location           	: 2600 Challenger Place, Oxnard, CA 93030 (USA)
Tags               	: [rwl][kar]
Filename uploaded  	: Jim_Hall_Kart_by_aTibi.zip
Version            	: v1.0
Tracks on zip file 	: Jim_Hall_LL.trk
			  Jim_Hall_LS.trk
			  Jim_Hall_SL.trk
			  Jim_Hall_SS.trk
Release Date       	: January 02, 2017
Email Address      	: andrea.tibiletti@me.com
Home Page          	: under construction
Other Tracks by Author  : 
Additional Credits      : Best regard
                                
===============================================================================================

* Play Information * and * Construction *

About track	  : Jim_Hall_LL.trk
Game Requirements : GeneRally 1.2c
Track Type        : karting 
Track Surface     : Tarmac
Track Length      : 773 meters
Rated Difficulty  : Mediaum
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 175
Water Level           : 50
View Angle            : 60 degrees
Rotation              : 0 degrees
Zoom                  : 95
Number of Checkpoints : 15
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 60 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release


About track	  : Jim_Hall_LS.trk
Game Requirements : GeneRally 1.2c
Track Type        : karting 
Track Surface     : Tarmac
Track Length      : 641 meters
Rated Difficulty  : Mediaum
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 175
Water Level           : 50
View Angle            : 60 degrees
Rotation              : 0 degrees
Zoom                  : 95
Number of Checkpoints : 14
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 60 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release
 


About track	  : Jim_Hall_LS.trk
Game Requirements : GeneRally 1.2c
Track Type        : karting 
Track Surface     : Tarmac
Track Length      : 648 meters
Rated Difficulty  : Mediaum
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 175
Water Level           : 50
View Angle            : 60 degrees
Rotation              : 0 degrees
Zoom                  : 95
Number of Checkpoints : 11
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 60 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release



About track	  : Jim_Hall_LS.trk
Game Requirements : GeneRally 1.2c
Track Type        : karting 
Track Surface     : Tarmac
Track Length      : 517 meters
Rated Difficulty  : Mediaum
Real-World Track  : Yes
Current Track Record : n/a
Base                  : New track from scratch
World Size            : 175
Water Level           : 50
View Angle            : 60 degrees
Rotation              : 0 degrees
Zoom                  : 95
Number of Checkpoints : 11
Editors Used          : GeneRally Track Editor 1.2c and Adobe Photoshop CS6 v.13.0
Build Time            : 60 days
Known Bugs            : n/a
Playtesters           : aT
Version History       : v1.0 - First release


* Copyright and Permissions *

Authors MAY use this track as a base to build additional tracks.

You MAY distribute this track, provided you include this file, with
no modifications. You MAY distribute this track in any electronic
format (BBS, Web Site, etc...) as long as you include this file intact.

* Where to get this track *

http://forum.generally-racers.com/viewforum.php?f=6&sid=941c163d86cd97bfc4fcde5767ed1cbf

Regards from Italy, Andrea Tibiletti